<?php // Template Name: Single Portfolio ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

    <!-- SINGLE PORTFOLIO -->
    <div class="single-portfolio">
        <!-- HEADER -->
        <header class="header">
            <div class="logo">
                <a href="home">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/jazzz.png">
                </a>
            </div>
            <nav class="navegacao">
                <div class="anterior">
                    <a href="<?php the_field('link-anterior'); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/seta.svg">
                        <span>anterior</span>
                    </a>
                </div>
                <div class="proximo">
                    <a href="<?php the_field('link-posterior'); ?>">
                        <span>próximo</span>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/seta.svg">
                    </a>
                </div>
                <div class="circulos">
                    <a href="https://www.jazzz.com.br/homologacao/sitejazzz/#portfolio">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/circles.svg">
                    </a>
                </div>
            </nav>
        </header>

        <!-- CONTEUDO -->
        <div class="conteudo">
            <div class="linha">
                <h1 class="titulo"><?php the_title()?></h1>
                <!-- <div></div> -->
            </div>
            <div class="area-imagem-destaque">
                <img src="<?php the_field('imagem-projeto-destaque'); ?>">
            </div>
            <div class="visao-geral">
                <h4 class="texto">Visão Geral</h4>
                <p class="texto"><?php the_field('texto-visao-geral'); ?></p>
                <div class="area-botao">
                    <a href="<?php the_field('link-site'); ?>" target="_blank">visitar site</a>
                </div>
            </div>
            <!-- LOOP -->
            <?php if(have_rows('imagens-portfolio')): while(have_rows('imagens-portfolio')) : the_row(); ?>
                <div class="area-imagem">
                    <img src="<?php the_sub_field('imagem-projeto'); ?>">
                </div>
            <?php endwhile; else : endif; ?>
            <!-- FIM DO LOOP -->

            <!-- <div class="area-botao">
                <button class="botao botao-principal btn btn-1">
                    <svg>
                        <rect x="0" y="0" fill="none" width="100%" height="100%"/>
                    </svg>
                    Enviar
                </button>
            </div> -->
        </div>
    </div>

<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>