$(function () {
  var cabecalho = $('.cabecalho');
  var links = $('.cabecalho .links');
  var menu = $('.cabecalho .menu');
  var redesSociais = $('.cabecalho .redes-sociais');

  $(window).scroll(function () {
    if ($(this).scrollTop() > 10) {
      cabecalho.addClass('efeitoCabecalho');
      links.addClass('efeitoLinks');
      menu.addClass('efeitoMenu');
      redesSociais.addClass('efeitoRedesSociais');
    } else {
      cabecalho.removeClass('efeitoCabecalho');
      links.removeClass('efeitoLinks');
      menu.removeClass('efeitoMenu');
      redesSociais.removeClass('efeitoRedesSociais');
    }
  });
});
