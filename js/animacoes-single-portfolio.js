// IMAGEM DESTAQUE
// const imagemDestaque = document.querySelector(
//   '.single-portfolio .conteudo .area-imagem-destaque',
// );
// gsap.from(imagemDestaque, {
//   scrollTrigger: {
//     trigger: imagemDestaque,
//     start: 'bottom bottom',
//   },
//   y: 20,
//   opacity: 0,
// });

// VISAO GERAL
const visaoGeral = document.querySelector(
  '.single-portfolio .conteudo .visao-geral',
);
gsap.from(visaoGeral, {
  scrollTrigger: {
    trigger: visaoGeral,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});

// AREA IMAGEM
const areaImagem = document.querySelector(
  '.single-portfolio .conteudo .area-imagem',
);
gsap.from(areaImagem, {
  scrollTrigger: {
    trigger: areaImagem,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
