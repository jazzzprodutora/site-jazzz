// ABERTURA - FAIXAS
// const faixas = document.querySelectorAll('.faixa');
// TweenMax.staggerTo(
//   faixas,
//   1,
//   {
//     width: 0,
//     display: 'none',
//   },
//   -0.4,
// );

// AGENCIA
// titulo
const tituloAgencia = document.querySelector(
  '.agencia .item .header-wrapper .titulo',
);
gsap.from(tituloAgencia, {
  scrollTrigger: {
    trigger: tituloAgencia,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
// linha
const linhaAgencia = document.querySelector(
  '.agencia .item .header-wrapper .linha',
);
gsap.from(linhaAgencia, {
  scrollTrigger: {
    trigger: linhaAgencia,
    start: 'bottom bottom',
  },
  x: 500,
  opacity: 0,
  delay: 1,
});
// texto
const texto = document.querySelectorAll('.agencia .item .texto');
gsap.from(texto, {
  scrollTrigger: {
    trigger: texto,
    start: 'bottom bottom',
  },
  y: 40,
  opacity: 0,
  delay: 0.4,
});

// SERVICOS
// titulo
const tituloServicos = document.querySelector(
  '.servicos .header-wrapper .titulo',
);
gsap.from(tituloServicos, {
  scrollTrigger: {
    trigger: tituloServicos,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
// linha
const linhaServicos = document.querySelector(
  '.servicos .header-wrapper .linha',
);
gsap.from(linhaServicos, {
  scrollTrigger: {
    trigger: linhaServicos,
    start: 'bottom bottom',
  },
  x: 500,
  opacity: 0,
  delay: 1,
});
// itens
const itensServicos = document.querySelectorAll('.servicos .itens .item');
gsap.from(itensServicos, {
  scrollTrigger: {
    trigger: itensServicos,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
  delay: 1,
});
// microfone
const microfoneServicos = document.querySelector('.servicos .area-imagem img');
gsap.from(microfoneServicos, {
  scrollTrigger: {
    trigger: microfoneServicos,
    start: 'bottom bottom',
  },
  x: -20,
  opacity: 0,
  delay: 1,
});
// botao
const botaoServicos = document.querySelector('.servicos .area-botao .botao');
gsap.from(botaoServicos, {
  scrollTrigger: {
    trigger: botaoServicos,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
  delay: 0.5,
});

// PORTFOLIO
// linha
const linhaPortfolio = document.querySelector(
  '.portfolio .header-wrapper .linha',
);
gsap.from(linhaPortfolio, {
  scrollTrigger: {
    trigger: linhaPortfolio,
    start: 'bottom bottom',
  },
  x: -500,
  opacity: 0,
  delay: 1,
});
// titulo
const tituloPortfolio = document.querySelector(
  '.portfolio .header-wrapper .titulo',
);
gsap.from(tituloPortfolio, {
  scrollTrigger: {
    trigger: tituloPortfolio,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
// links tabs
const linksTabs = document.querySelectorAll('.nav-tabs .nav-link');
gsap.from(linksTabs, {
  scrollTrigger: {
    trigger: linksTabs,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
// itens
const itensPortfolio = document.querySelectorAll('.portfolio .itens .item');
gsap.from(itensPortfolio, {
  scrollTrigger: {
    trigger: itensPortfolio,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
  delay: 1,
});
// botao
const botaoPortfolio = document.querySelector('.portfolio .area-botao .botao');
gsap.from(botaoPortfolio, {
  scrollTrigger: {
    trigger: botaoPortfolio,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
  delay: 0.5,
});

// CLIENTES
// titulo
const tituloClientes = document.querySelector(
  '.clientes .header-wrapper .titulo',
);
gsap.from(tituloClientes, {
  scrollTrigger: {
    trigger: tituloClientes,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
// linha
const linhaClientes = document.querySelector(
  '.clientes .header-wrapper .linha',
);
gsap.from(linhaClientes, {
  scrollTrigger: {
    trigger: linhaClientes,
    start: 'bottom bottom',
  },
  x: 500,
  opacity: 0,
  delay: 1,
});
// itens
const itensClientes = document.querySelectorAll('.clientes .itens .item');
gsap.from(itensClientes, {
  scrollTrigger: {
    trigger: itensClientes,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
  delay: 1,
});
// mensagem
// const mensagemClientes = document.querySelectorAll('.clientes .mensagem');
// gsap.from(mensagemClientes, {
//   scrollTrigger: {
//     trigger: mensagemClientes,
//     start: 'bottom bottom',
//   },
//   y: 20,
//   opacity: 0,
//   delay: 1,
// });

// CONTATO
// formulario
const textoContato = document.querySelector('.contato .formulario .texto');
gsap.from(textoContato, {
  scrollTrigger: {
    trigger: textoContato,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
// campo
const campoFormulario = document.querySelectorAll(
  '.contato .formulario .campo',
);
gsap.from(campoFormulario, {
  scrollTrigger: {
    trigger: campoFormulario,
    start: 'bottom bottom',
  },
  y: 20,
  opacity: 0,
});
