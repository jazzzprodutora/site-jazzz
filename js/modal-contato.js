const abreModal = document.querySelectorAll('.modal-contato');
const contato = document.querySelector('.contato');
const faixa = document.querySelector('.faixa-contato');
const closeModal = document.querySelector('.close-modal');
abreModal.forEach(function (modal) {
  modal.addEventListener('click', () => {
    contato.classList.add('efeito-modal');
    faixa.classList.add('efeito-faixa');
  });
});
closeModal.addEventListener('click', () => {
  contato.classList.remove('efeito-modal');
});

console.log(abreModal);
