$(function () {
  var cabecalho = $('.single-portfolio .header');
  var logo = $('.single-portfolio .header .logo a img');

  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      cabecalho.addClass('efeitoCabecalhoSinglePortfolio');
      logo.addClass('efeitoLogoSinglePortfolio');
    } else {
      cabecalho.removeClass('efeitoCabecalhoSinglePortfolio');
      logo.removeClass('efeitoLogoSinglePortfolio');
    }
  });
});
