<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <!-- SEO -->
    <title><?php bloginfo('name'); ?> - <?php the_title()?></title>
    <meta name="author" content="Jazzz Agência Digital - Hugo Queiroz">
    <meta name="description" content="">
    <meta name="keywords" content=""/>
    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/icons/favicon.png">
    <meta name="theme-color" content="#fff">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- SLICK CAROUSEL -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick-theme.css"/>
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/animate.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/scss/estilo.css">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@200;400;700&display=swap" rel="stylesheet">
    <!-- JS CAPTCHA -->
    <script src="https://www.google.com/recaptcha/api.js"></script>

    <!-- WP -->
    <?php wp_head(); ?>
</head>
<body>