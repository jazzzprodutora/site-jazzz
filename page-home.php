<?php // Template Name: Home ?>

<!-- CHAMA O HEADER WP -->
<?php get_header(); ?>

<!-- LAYER ANIMACAO -->
<div class="abertura">
    <div class="faixa"></div>
    <div class="faixa"></div>
</div>

<main class="principal" id=principal>
    <!-- HEADER -->
    <header class="header">
        <!-- CABECALHO -->
        <?php require 'templates/cabecalho.php' ?>

        <!-- BANNER -->
        <div class="banner">

            <div class="carousel-banner itens">
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner1.jpg">
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner2.jpg">
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner3.jpg">
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner4.jpg">
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner5.jpg">
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner6.jpg">
                </div>
                <div class="item">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/banner7.jpg">
                </div>
            </div>

            <div class="mouse">
                <a href="#agencia" class="scroll">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/mouse.png">
                </a>
            </div>
        </div>
    </header>    
</main>

<!-- AGENCIA -->
<section class="agencia" id="agencia">
    <div class="bg">
        <div class="itens">
            <div class="item">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/agencia.png" alt="Jazzz Agência Digital">
            </div>
            <div class="item">
                <div class="header-wrapper">
                    <h2 class="titulo">Agência</h2>
                    <div class="linha"></div>
                </div>
                <p class="texto">Somos uma agência com <span>15 anos de experiência</span>, atravessando um tempo de mudanças significativas para comunicação, na qual a criatividade e inovação, com o avanço das tecnologias e demandas emergentes, fortaleceram permanentemente nosso dna de adaptação.</p>
                <p class="texto">Em consequência disso, nos tornamos uma agência inovadora que acompanha as tendências de forma singular e transformamos marcas em cases de sucesso, sendo referência no mercado de agências digitais como acessoria de comunicação e desenvolvedores web.</p>
                <p class="texto">Nossos talentos contam com um conjunto de conhecimentos, tecnologias e recursos no objetivo de posicionar as empresas com competência e propriedade, sempre dispostos a entregar nosso melhor com profissionalismo.</p>
                <p class="texto">Nos habituamos a surpreender.</p>
            </div>
        </div>
    </div>
</section>

<!-- SERVICOS -->
<section class="servicos" id="servicos">
    <div class="header-wrapper">
        <h2 class="titulo">Serviços</h2>
        <div class="linha"></div>
    </div>
    <div class="itens">
        <div class="item">
            <h3 class="titulo">Design</h3>
            <p class="texto">Criação de Marcas<br>Identidades e manuais</p>
        </div>
        <div class="item">
            <h3 class="titulo">Estratégia</h3>
            <p class="texto">Comunicação<br>Empresarial</p>
        </div>
        <div class="item">
            <h3 class="titulo">Social Media</h3>
            <p class="texto">Presença digital<br>nas Redes Sociais</p>
        </div>
        <div class="item">
            <h3 class="titulo">Mídia Digital</h3>
            <p class="texto">Estratégias para<br>Engajamento</p>
        </div>
        <div class="item">
            <h3 class="titulo">Web Design</h3>
            <p class="texto">Criação de Sites,<br>Blogs e Landing Pages</p>
        </div>
        <div class="item">
            <h3 class="titulo">E-Commerce</h3>
            <p class="texto">Desenvolvimento<br>e Consultoria</p>
        </div>
        <div class="item">
            <h3 class="titulo">SEO</h3>
            <p class="texto">Otimização web e<br>ranking no Google</p>
        </div>
        <div class="item">
            <h3 class="titulo">Produção</h3>
            <p class="texto">Audio-Visual e<br>Fotografia</p>
        </div>
    </div>
    <div class="area-imagem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/microfone.png">
    </div>
    <div class="area-botao">
        <button class="botao botao-principal btn btn-1 modal-contato">
            <svg>
                <rect x="0" y="0" fill="none" width="100%" height="100%"/>
            </svg>
            Entrar em contato
        </button>
    </div>
</section>


<!-- PORTFOLIO -->
<section class="portfolio" id="portfolio">
    <div class="header-wrapper">
        <h2 class="titulo">Portfólio</h2>
        <div class="linha"></div>
    </div>

    <!-- PILLS -->
    <nav class="nav nav-tabs" id="nav-tab" role="tablist">
        <!-- <a class="nav-link active" id="nav-sites-tab" data-bs-toggle="tab" href="#nav-sites" role="tab" aria-controls="nav-sites" aria-selected="true">Sites</a> -->
        <a class="nav-link active" id="nav-eCommerces-tab" data-bs-toggle="tab" href="#nav-eCommerces" role="tab" aria-controls="nav-eCommerces" aria-selected="false">e-Commerces</a>
        <a class="nav-link" id="nav-marcas-tab" data-bs-toggle="tab" href="#nav-marcas" role="tab" aria-controls="nav-marcas" aria-selected="false">Marcas</a>
        <!-- <a class="nav-link" id="nav-redesSociais-tab" data-bs-toggle="tab" href="#nav-redesSociais" role="tab" aria-controls="nav-redesSociais" aria-selected="false">Redes Sociais</a> -->
    </nav>
    <!-- CONTEUDO PILLS -->
    <div class="tab-content" id="nav-tabContent">

        <!-- SITES -->
        <div class="itens tab-pane fade" id="nav-sites" role="tabpanel" aria-labelledby="nav-sites-tab">
            <!-- ... -->
            <?php
                $args = array (
                    'post_type' => 'portfolio', //Pega os post types no array para ser mostrado nos post
                    'categoria' => 'sites',
                    'order' => 'ASC',
                    'posts_per_page'=> 9
                );
                $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <div class="item">
                <a href="<?php the_permalink();?>">
                    <img src="<?php the_field('thumbnail'); ?>" alt="<?php the_title()?>">
                    <div class="overlay"></div>
                </a>
            </div>

            <?php endwhile; else: endif; ?>
            <!-- ... -->
        </div>

        <!-- E-COMMERCES -->
        <div class="itens tab-pane fade show active" id="nav-eCommerces" role="tabpanel" aria-labelledby="nav-eCommerces-tab">
            <!-- ... -->
            <?php
                $args = array (
                    'post_type' => 'portfolio', //Pega os post types no array para ser mostrado nos post
                    'categoria' => 'e-commerces',
                    'order' => 'ASC',
                    'posts_per_page'=> 9
                );
                $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <div class="item">
                <a href="<?php the_permalink();?>">
                    <img src="<?php the_field('thumbnail'); ?>" alt="<?php the_title()?>">
                    <div class="overlay">
                        <img src="<?php the_field('imagem-projeto-destaque'); ?>">
                    </div>
                </a>
            </div>

            <?php endwhile; else: endif; ?>
            <!-- ... -->
        </div>

        <!-- MARCAS -->
        <div class="itens tab-pane fade" id="nav-marcas" role="tabpanel" aria-labelledby="nav-marcas-tab">
            <!-- ... -->
            <?php
                $args = array (
                    'post_type' => 'portfolio', //Pega os post types no array para ser mostrado nos post
                    'categoria' => 'marcas',
                    'order' => 'ASC',
                    'posts_per_page'=> 9
                );
                $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <div class="item">
                <a href="<?php the_permalink();?>">
                    <img src="<?php the_field('thumbnail'); ?>" alt="<?php the_title()?>">
                    <div class="overlay"></div>
                </a>
            </div>

            <?php endwhile; else: endif; ?>
            <!-- ... -->
        </div>

        <!-- REDES SOCIAIS -->
        <div class="itens tab-pane fade" id="nav-redesSociais" role="tabpanel" aria-labelledby="nav-redesSociais-tab">
            <!-- ... -->
            <?php
                $args = array (
                    'post_type' => 'portfolio', //Pega os post types no array para ser mostrado nos post
                    'categoria' => 'redes-sociais',
                    'order' => 'ASC',
                    'posts_per_page'=> 9
                );
                $the_query = new WP_Query ( $args );
            ?>
            <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

            <div class="item">
                <a href="<?php the_permalink();?>">
                    <img src="<?php the_field('thumbnail'); ?>" alt="<?php the_title()?>">
                    <div class="overlay"></div>
                </a>
            </div>

            <?php endwhile; else: endif; ?>
            <!-- ... -->
        </div>
    </div>
    <!-- ... -->

    <div class="area-botao">
        <button class="botao botao-principal btn btn-1 modal-contato">
            <svg>
                <rect x="0" y="0" fill="none" width="100%" height="100%"/>
            </svg>
            Entrar em contato
        </button>
    </div>
</section>

<!-- CLIENTES -->
<section class="clientes" id="clientes">
    <div class="header-wrapper">
        <h2 class="titulo">Clientes</h2>
        <div class="linha"></div>
    </div>
    <div class="itens">
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/asa.png" alt="ASA">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/oq.png" alt="OQ Bebidas">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/inova.png" alt="Inova">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/define.png" alt="Define">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/procenge.png" alt="Procenge">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/dr-raphael.png" alt="Dr Raphael Sampaio">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/rec.png" alt="REC Produtores">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/nx.png" alt="NX Endodontia">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/andre-caricio.png" alt="André Carício">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/casa-plantio.png" alt="Casa do Plantio">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/prospec.png" alt="Prospec">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/mec.png" alt="MEC Varejão">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/guarnieri.png" alt="Guarnieri Engenharia">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/sigillo.jpg" alt="Sigillo">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/carol.png" alt="Carol Esportes">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/museu.png" alt="Museu do Estado">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/love-burger.png" alt="Love Burger">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/bework.png" alt="BeWork">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/hal.png" alt="Hal Paisagismo">
        </div>
        <div class="item">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/clientes/ags.png" alt="AGS Construtora">
        </div>
    </div>
    <div class="mensagem" id="contato">
        <p class="texto">Estamos prontos para atender sua marca e sua empresa.</p>
        <div class="area-botao">
            <button class="botao botao-principal btn btn-1 modal-contato">
                <svg>
                    <rect x="0" y="0" fill="none" width="100%" height="100%"/>
                </svg>
                Entrar em contato
            </button>
        </div>
        <div class="info">
            <a href="https://goo.gl/maps/QuudywC9hRACZVaW9" target="_blank" class="endereco">Praça de Casa Forte, 465, Casa Forte, Recife/PE, CEP: 52061-420</a>
            <a href="tel:81994494637" class="telefone">81 9 9449-4637</a>
            <a href="mailto:falecom@jazzz.com.br" class="email">falecom@jazzz.com.br</a>
            <div class="redes-sociais">
                <a href="https://www.instagram.com/jazzzagenciadigital/" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram-preto.svg" alt="Instagram">
                </a>
                <a href="https://www.linkedin.com/company/jazzz-web-&-design" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/linkedin-preto.svg" alt="LinkedIn">
                </a>
                <a href="https://api.whatsapp.com/send?phone=558194494637&text=Olá, gostaria de fazer um orçamento." target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp-preto.svg" alt="Whatsapp">
                </a>
            </div>
        </div>
    </div>
</section>

<!-- CONTATO -->
<section class="contato">
    <div class="faixa-contato"></div>
    <div class="close-modal">
        <div></div>
        <div></div>
    </div>
    <div class="itens">
        <div class="item bg"></div>
        <div class="item formulario">
            <div class="area-texto">
                <p class="texto">Seja bem vindx!</p>
                <p class="texto">Somos a Jazzz Agência Digital e estamos prontos para lhe atender.</p>
                <p class="texto">Deixe seus dados abaixo e retornaremos seu contato.</p>
            </div>
            <form action="">
                <?php echo do_shortcode( '[contact-form-7 id="120" title="Form1"]' ); ?>
            </form>
        </div>
    </div>
</section>


<!-- CHAMA O RODAPE -->
<?php require 'footer.php' ?>