<!-- CABECALHO -->
<nav class="cabecalho">
    <div class="logo">
        <a href="#principal" class="scroll">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/imgs/jazzz.png" alt="Jazzz">
        </a>
    </div>
    <div class="links">
        <a href="#agencia" class="link scroll">Agência<span class="linha"></span></a>
        <a href="#servicos" class="link scroll">Serviços<span class="linha"></span></a>
        <a href="#portfolio" class="link scroll">Portfólio<span class="linha"></span></a>
        <a href="#clientes" class="link scroll">Clientes<span class="linha"></span></a>
        <a href="#contato" class="link scroll">Contatos<span class="linha"></span></a>
    </div>
    <div class="redes-sociais">
        <a href="https://www.instagram.com/jazzzagenciadigital/" target="_blank">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.svg" alt="Instagram">
        </a>
        <a href="https://www.linkedin.com/company/jazzz-web-&-design" target="_blank">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/linkedin.svg" alt="LinkedIn">
        </a>
        <a href="https://api.whatsapp.com/send?phone=558194494637&text=Olá, gostaria de fazer um orçamento." target="_blank">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.svg" alt="Whatsapp">
        </a>
    </div>

    <!-- MENU COMPRIMIDO -->
    <div class="menu">
        <div id="linha1"></div>
        <div id="linha2"></div>
        <div id="linha3"></div>
        <div class="menu-conteudo">
            <div class="links-comprimidos">
                <a href="#agencia" class="link scroll">Agência<span class="linha"></span></a>
                <a href="#servicos" class="link scroll">Serviços<span class="linha"></span></a>
                <a href="#portfolio" class="link scroll">Portfólio<span class="linha"></span></a>
                <a href="#clientes" class="link scroll">Clientes<span class="linha"></span></a>
                <a href="#contato" class="link scroll">Contatos<span class="linha"></span></a>
            </div>
            <div class="redes-sociais-comprimidas">
                <a href="https://www.instagram.com/jazzzagenciadigital/" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/instagram.svg" alt="Instagram">
                </a>
                <a href="https://www.linkedin.com/company/jazzz-web-&-design" target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/linkedin.svg" alt="LinkedIn">
                </a>
                <a href="https://api.whatsapp.com/send?phone=558194494637&text=Olá, gostaria de fazer um orçamento." target="_blank">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/icons/whatsapp.svg" alt="Whatsapp">
                </a>
            </div>
        </div>
    </div>
</nav>