
	
	<!-- JS BOOTSTRAP -->
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

	<!-- JS SCROLL -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

	<!-- JS GSAP -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollTrigger.min.js"></script>

	<!-- SLICK -->
    <script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/slick/slick.min.js"></script>

	<!-- JS -->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/cabecalho-single-portfolio.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/cabecalho.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/menu.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/animacoes-single-portfolio.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/animacoes.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/scroll.js"></script>
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/modal-contato.js"></script>


	<script>
        $('.carousel-banner').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
			autoplay: true,
            fade: true,
            autoplaySpeed: 10000,
            pauseOnFocus: false,
            pauseOnHover: false,
            speed: 2000,
            arrows: false,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
    </script>

    <script>
        $('.carousel-portfolio-eCommerces').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
			autoplay: true,
            fade: true,
            autoplaySpeed: 10000,
            pauseOnFocus: false,
            pauseOnHover: false,
            speed: 2000,
            arrows: false,
            responsive: [
                {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
                },
                {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                },
                {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
                }
            ]
        });
    </script>


	<?php wp_footer(); ?>
</body>
</html>
